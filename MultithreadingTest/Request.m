//
//  Request.m
//  MultithreadingTest
//
//  Created by Peter Prokop on 4/6/13.
//  Copyright (c) 2013 Kaspersky. All rights reserved.
//

#import "Request.h"

@implementation Request

@synthesize number;

- (id)init
{
    self = [super init];
    if(self)
    {
        self.number = arc4random() % 20 + 1;
    }
    return self;
}

@end
