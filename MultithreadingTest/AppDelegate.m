//
//  AppDelegate.m
//  MultithreadingTest
//
//  Created by Peter Prokop on 4/6/13.
//  Copyright (c) 2013 Kaspersky. All rights reserved.
//

#define NUM_OF_THREADS 10
#define TIME_TO_EXECUTE 30
#define INITIAL_QUEUE_CAPACITY 100
#define INITIAL_QUEUE_NUM_OF_REQUESTS 5
#define INTERVAL_REQUEST_PROCESSING_MAX_SLEEP 15
#define INTERVAL_REQUEST_SPAWN_MAX_SLEEP 1

#import "AppDelegate.h"
#import "Request.h"

@implementation AppDelegate
{
    Stopper _stopper;
    NSMutableArray* _queue;
    NSMutableArray* _threads;
}
- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _stopper = Stopper_greenLight;
    
    // Initialize queue
    _queue = [[NSMutableArray alloc] initWithCapacity:INITIAL_QUEUE_CAPACITY];
    for(NSInteger i=0; i<INITIAL_QUEUE_NUM_OF_REQUESTS; i++)
    {
        Request* request = [self getRequest:_stopper];
        [_queue addObject:request];
        [request release];
    }
    
    // Shut down after 30 seconds
    [NSTimer scheduledTimerWithTimeInterval:TIME_TO_EXECUTE
                                     target:self
                                   selector:@selector(stop)
                                   userInfo:nil
                                    repeats:NO];
    
    // !!!: Optional - start generating requests
    [NSThread detachNewThreadSelector:@selector(generateRequests)
                             toTarget:self
                           withObject:nil];
    
    // Start processing requests
    _threads = [[NSMutableArray alloc] initWithCapacity:NUM_OF_THREADS];
    for(NSInteger i=0; i<NUM_OF_THREADS; i++)
    {
        NSThread* thread = [[NSThread alloc] initWithTarget:self
                                                   selector:@selector(newThread)
                                                     object:nil];
        [thread start];
        [_threads addObject:thread];
        [thread release];
    }
    
    return YES;
}

- (void)generateRequests
{
    while (_stopper == Stopper_greenLight)
    {
        NSTimeInterval sleepInterval = arc4random()%(INTERVAL_REQUEST_SPAWN_MAX_SLEEP * 100) / 100.0;
        NSLog(@"Interval till next request is generated: %.2f seconds", sleepInterval);
        [NSThread sleepForTimeInterval:sleepInterval];
        
        Request* request = [self getRequest:_stopper];
        if(request != nil)
        {
            [_queue addObject:request];
            [request release];
        }
    }
    
    NSLog(@"Request generating thread is finished");
}

- (void)newThread
{
    while (_stopper == Stopper_greenLight)
    {
        Request* request = nil;
        
        // This is a critical section - we need semaphore
        @synchronized(self)
        {
            if([_queue count] != 0)
            {
                NSLog(@"Taking request from queue. Current queue lenght: %i", [_queue count]);
                request = [[_queue objectAtIndex:0] retain];
                [_queue removeObject:request];
            }
        }
        
        if(request != nil)
        {    
            [self processRequest:request
                     withStopper:_stopper];
            [request release];
        }
    }
}

- (void)stop
{
    NSLog(@"Stopping...");
    @synchronized(self)
    {
        _stopper = Stopper_redLight;
        [_queue removeAllObjects];
        [_queue release];
        _queue = nil;
    }
    
    for(NSThread* thread in _threads)
        [thread cancel];
    [_threads removeAllObjects];
    [_threads release];
    
    [NSThread sleepForTimeInterval:1];
    exit(0);
}

- (Request*)getRequest:(Stopper) stopper
{
    if(stopper == Stopper_greenLight)
        return [[Request alloc] init];
    
    return nil;
}

- (void)processRequest:(Request*)request
           withStopper:(Stopper) stopper
{
    if(stopper == Stopper_redLight)
    {
        NSLog(@"Red light - request is not processed");
        return;
    }
    
    NSTimeInterval sleepInterval = arc4random()%(INTERVAL_REQUEST_PROCESSING_MAX_SLEEP * 100) / 100.0;
    NSLog(@"Sleeping for ~%.2f seconds...", sleepInterval);
    
    for(NSInteger i=0; i<100; i++)
    {
        if([[NSThread currentThread] isCancelled])
        {
            NSLog(@"Thread is cancelled");
            [request release];
            [NSThread exit];
        }
        [NSThread sleepForTimeInterval:sleepInterval/100.0];
    }
    
    long double factorial = 1;
    for(NSInteger i=1; i<=request.number; i++)
    {
        if([[NSThread currentThread] isCancelled])
        {
            NSLog(@"Thread is cancelled");
            [request release];
            [NSThread exit];
        }
        factorial *= i;
    }
    
    NSLog(@"%i factorial is %.0Lf", request.number, factorial);
}

@end