//
//  Request.h
//  MultithreadingTest
//
//  Created by Peter Prokop on 4/6/13.
//  Copyright (c) 2013 Kaspersky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Request : NSObject

@property (readwrite, assign) NSInteger number;

@end
