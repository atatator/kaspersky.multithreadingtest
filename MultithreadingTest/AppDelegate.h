//
//  AppDelegate.h
//  MultithreadingTest
//
//  Created by Peter Prokop on 4/6/13.
//  Copyright (c) 2013 Kaspersky. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    Stopper_greenLight,
    Stopper_redLight
} Stopper;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
